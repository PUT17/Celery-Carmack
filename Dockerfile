# use base python image with python 2.7
FROM python:3.6

# add requirements.txt to the image
# ADD requirements.txt /app/requirements.txt
# set working directory to /app/
# WORKDIR /app/
# ADD ./app/tasks.py ./app/tasks.py
# COPY /app /app

# install python dependencies
#RUN pip install -r requirements.txt

# create unprivileged user
#RUN adduser --disabled-password --gecos '' myuser  


ADD . /app
WORKDIR /app
RUN pip install -r requirements.txt

# CMD ["celery", "worker", "-A", "./tasks", "-l", "info"]