# Table of Contents
1. [Celery](#celery)
2. [Simple Celery application](#simple-celery-application)
3. [How to use](#how-to-use)
3. [Architectures](#architectures)

# Celery 
Celery is an asynchronous task queue/job queue based on distributed message passing.  
It is focused on real-time operation, but supports scheduling as well.  
#### Celery site : [Celery](http://www.celeryproject.org/, "Celery site")

# Simple Celery application
Simple application uses a Celery library to get histogram of few image. Images comes from URL.
Histogram was create and stored in database [mLab](https://mlab.com/ "mLab site")(MongoDB)

Used libraries:
- celery
- redis as broker
- pillow to create histogram of image
- validators to valid URL
- pymongo to connect with MongoDB
- requests to get image from URL

# How to use

1. Obtain repository with ```git clone https://gitlab.com/PUT17/Celery-Carmack.git```
2. Open ```CDM``` / ```terminal```
3. Enter to ```~/Celery-Carmack/worker```
4. Type ```docker-compose up``` or ```docker-compose up --scale celery_worker=N``` where N is number of celery workers 
5. Go to ```Celery-Cermack``` folder
6. Type ```docker-compose up```

### You have to have docker installed on your machine. 


# Architectures

Main directory contains:
- ```docker-compose.yml```, ```Dockerfile```,  ```requirements.txt``` files to run container
- ```links.txt``` contains in each line one URL to image
- ```run_task.py``` python program to delegate task to celery queue

Worker directory conatains:
- as above ```docker-compose.yml```, ```Dockerfile```,  ```requirements.txt``` files to run container
- ```task.py``` file with celery configuration and available task- 

Database record structure:
- "filename": "4173232245_88186f3872_o_d",
- "extension": "jpg",
- "url": "https://farm3.staticflickr.com/2766/4173232245_88186f3872_o_d.jpg",
- "width": 1500,
- "height": 986,
- "worker_id": 1,
- "data_time": "12:39PM on February 14, 2018",
- "executing_time": 1.068049430847168,
- "red": [list of values]
- "blue": [list of values]
- "green": [list of values]