import time

from celery import group

from worker.tasks import create_histogram_url

if __name__ == "__main__":
    print("Waiting for start all worker(5s)")
    time.sleep(5)

    result = list()
    with open('links.txt', 'r') as reader:
        for link in reader.readlines():
            result.append(create_histogram_url.s(str(link)))
                          
    tasks_group = group(result)

    start = time.time()
    result = tasks_group.apply_async()
    while not result.ready():
        pass
    stop = time.time() - start
    print("Tasks finished in %f s" % stop)

