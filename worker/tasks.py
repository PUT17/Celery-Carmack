import matplotlib as mpl

mpl.use('Agg')

import validators
import requests
import string
import time
import datetime
from PIL import Image
from celery import Celery
from pymongo import MongoClient
import socket
import random
from celery import uuid
from io import BytesIO
from billiard import current_process

app = Celery('tasks', broker='redis://redis:6379', backend='redis://redis:6379/0')


app.conf.broker_transport_options = {'visibility_timeout': 3600}  # 1 hour.

random.seed(time.time())
ID_WORKER = ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))
ID_WORKER += socket.gethostname()


@app.task(name='task.create_histogram_file')
def create_histogram_url(url):
    start = time.time()
    if not validators.url("http://google.com"):
        return 'Not a url!'
    
    client = MongoClient("mongodb://cosfajnego:cosfajnego@ds129428.mlab.com:29428/fajnie")
    db = client['fajnie']
    collection = db['test']

    response = requests.get(url)
    image = Image.open(BytesIO(response.content))
    histogram = image.histogram()
    
    l1 = histogram[0:256]
    l2 = histogram[256:512]
    l3 = histogram[512:768]

    width, height = image.size
    filename = url.split('/')[-1]
    filename = filename.split('&h=')[-1]
    filename = filename.split('.')[0]
    filename = filename.rstrip()
    extension = url.split('/')[-1]
    extension = extension.split('.')[-1]
    extension = extension.split('&h')[0].rstrip()
    extension = extension.rstrip()
    worker_id = current_process().index

    data = {"filename": filename,
            "extension": extension,
            "url": url.rstrip(),
            "width": width,
            "height": height,
            "worker_id": worker_id,
            "data_time": datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y"),
            "executing_time": time.time() - start,
            "red": l1,
            "green": l2,
            "blue": l3
            }
            
    collection.insert_one(data)
